// @flow

import React, { type ChildrenArray, type Node } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';

import Header from 'components/Header';

type Props = {
  children: ChildrenArray<Node>,
  title?: string
};

const Layout = ({ children, title }: Props) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Header />

    {children}

    <footer>
      {'I`m here to stay'}
    </footer>
  </div>
);

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  title: PropTypes.string,
};

Layout.defaultProps = {
  title: 'my-site.com',
};

export default Layout;
